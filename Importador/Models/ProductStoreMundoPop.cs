﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Importador.Models
{
    public class ProductStoreMundoPop
    {
        public int id { get; set; }
        public string sku { get; set; }
        public string name { get; set; }
        public int attribute_set_id { get; set; }
        public double price { get; set; }
        public int status { get; set; }
        public int visibility { get; set; }
        public string type_id { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public double weight { get; set; }
        public Extension extension_attributes { get; set; }
        public List<ProdLinks> product_links { get; set; }
        public List<Options> options { get; set; }
        public List<Galery> media_gallery_entries { get; set; }
        public List<Prices> tier_prices { get; set; }
        public List<Attributes> custom_attributes { get; set; }
    }

    public class Extension
    {
        public List<int> website_ids { get; set; }

        public List<CategoryLinks> category_links { get; set; }
    }
    public class CategoryLinks
    {
        public int position { get; set; }
        public string category_id { get; set; }
    }
    public class ProdLinks
    { }
    public class Options
    { }

    public class Galery
    {
        public int id { get; set; }
        public string media_type { get; set; }
        public string label { get; set; }
        public int position { get; set; }
        public bool disabled { get; set; }
        public string[] types { get; set; }
        public string file { get; set; }
    }
    public class Prices
    { }

    public class Attributes
    {
        public string attribute_code { get; set; }
        public string value { get; set; }
    }


}
